FROM python:3.6.3-alpine
ENV PYTHONUNBUFFERED 1
RUN mkdir /app
WORKDIR /app
ADD requirements.txt /app/
RUN apk update && \
    apk add --no-cache libstdc++ postgresql-libs libxslt-dev jpeg-dev && \
    apk add --no-cache --virtual .build-deps python3-dev g++ make swig linux-headers postgresql-dev git libffi-dev zlib-dev && \
    pip3 install -r requirements.txt --src /src && \
    apk del .build-deps

ADD . /app/

