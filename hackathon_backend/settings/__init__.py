from .base import *

try:
    from .dev import *

    print("Dev loaded")
except ImportError as e:
    from .production import *
    print(e)
    print("Production loaded")
