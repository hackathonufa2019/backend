from rest_framework_jwt.authentication import JSONWebTokenAuthentication


class UserIPAddressAuthentication(JSONWebTokenAuthentication):
    def authenticate(self, request):
        return super().authenticate(request)
