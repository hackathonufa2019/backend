from rest_framework.response import Response
from rest_framework.views import APIView

from minsel.models import DocumentType, DocumentToUser
from user.api.serializers import UserCreateSerializer
from user.models import User
from user.utils import get_jwt_token_for_user


class RegisterView(APIView):
    permission_classes = ()
    authentication_classes = ()

    def post(self, request, *args, **kwargs):
        serializer = UserCreateSerializer(data=request.data)
        response = {}
        if serializer.is_valid(raise_exception=True):
            email = serializer.validated_data['email']
            password = serializer.validated_data['password']
            user = User.objects.create_user(email=email, password=password)
            response = {'token': get_jwt_token_for_user(user)}

        return Response(response)


class CreateUserView(APIView):
    permission_classes = ()
    authentication_classes = ()

    def post(self, request, *args, **kwargs):
        password = User.objects.make_random_password()
        email = User.objects.make_random_email()
        extras = {
            "first_name": "",
            "last_name": ""
        }
        user = User.objects.create_user(email=email, password=password, **extras)
        passport, _ = DocumentType.objects.get_or_create(name="Паспорт")
        DocumentToUser.objects.create(doc_type=passport, user=user, document_data={})

        response = {'token': get_jwt_token_for_user(user)}

        return Response(response)


class CreateAdminUserView(APIView):
    permission_classes = ()
    authentication_classes = ()

    def post(self, request, *args, **kwargs):
        print("ADMIN")
        password = User.objects.make_random_password()
        email = User.objects.make_random_email()
        extras = {
            "first_name": "Admin",
            "last_name": ""
        }
        user = User.objects.create_superuser(email=email, password=password, **extras)
        response = {'token': get_jwt_token_for_user(user)}

        return Response(response)
