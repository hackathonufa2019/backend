from django.urls import path
from rest_framework_jwt.views import ObtainJSONWebToken

from .views import RegisterView, CreateUserView, CreateAdminUserView

urlpatterns = [
    path('login', ObtainJSONWebToken.as_view()),
    path('register', RegisterView.as_view()),
    path('create_user', CreateUserView.as_view()),
    path('create_admin_user', CreateAdminUserView.as_view()),
]
