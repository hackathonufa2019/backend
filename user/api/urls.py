from django.urls import path

from .views import UserMeRetrieveAPIView, UsersRetrieveAPIView

urlpatterns = [
    path('me', UserMeRetrieveAPIView.as_view()),
    path('users', UsersRetrieveAPIView.as_view()),

]
