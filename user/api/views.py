from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.permissions import IsAuthenticated

from authorization.auth import UserIPAddressAuthentication
from minsel.api.serializers import UserDocSerializer
from .serializers import UserMeSerializer
from ..models import User


class UserMeRetrieveAPIView(RetrieveAPIView):
    serializer_class = UserMeSerializer
    authentication_classes = (UserIPAddressAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        return self.request.user


class UsersRetrieveAPIView(ListAPIView):
    serializer_class = UserDocSerializer
    authentication_classes = (UserIPAddressAuthentication,)

    def get_queryset(self):
        return User.objects.filter(is_superuser=False).exclude(first_name='')
