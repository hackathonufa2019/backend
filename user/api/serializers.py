from rest_framework import serializers

from user.models import User


class UserMeSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('inn',
                  'first_name',
                  'last_name',
                  'id',
                  'middle_name')


class UserCreateSerializer(serializers.ModelSerializer):
    password = serializers.CharField()

    class Meta:
        model = User
        fields = ('email', 'password')
