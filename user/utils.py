from rest_framework_jwt.utils import *


def get_jwt_token_for_user(user):
    payload = jwt_payload_handler(user)
    return jwt_encode_handler(payload)
