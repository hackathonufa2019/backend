from django.urls import path

from .views import UserDocumentsRetrieveAPIView, UserCommonInformationSaveView

urlpatterns = [
    path('user_docs/<int:user_id>/', UserDocumentsRetrieveAPIView.as_view()),
    path('user_docs/<int:user_id>/<int:page_id>', UserDocumentsRetrieveAPIView.as_view()),
    path('user_docs/<int:id>/<int:page_id>/<int:subpage_id>', UserDocumentsRetrieveAPIView.as_view()),
    path('saveCommon', UserCommonInformationSaveView.as_view())
]
