from rest_framework import serializers

from user.models import User
from ..models import DocumentToUser


class DocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = DocumentToUser
        fields = "__all__"


class UserDocSerializer(serializers.ModelSerializer):
    documents = serializers.SerializerMethodField()

    class Meta:
        fields = ('id',
                  'first_name',
                  'middle_name',
                  'last_name',
                  'inn',
                  'status',
                  'documents',)
        model = User

    def get_documents(self, instance: User):
        doc = instance.documents \
            .filter(doc_type__name='Паспорт') \
            .first()
        data = {
            "passport": DocumentSerializer(doc).data
        }
        return data
