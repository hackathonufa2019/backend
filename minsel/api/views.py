from rest_framework.generics import RetrieveAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from user.api.serializers import UserMeSerializer
from user.models import User
from .serializers import UserDocSerializer, DocumentSerializer
from ..models import DocumentToUser


class UserDocumentsRetrieveAPIView(RetrieveAPIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    serializer_class = UserDocSerializer

    lookup_field = 'id'
    lookup_url_kwarg = 'id'

    queryset = User.objects.all()

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['page_id'] = self.kwargs.get('page_id')
        context['subpage_id'] = self.kwargs.get('subpage_id')

        return context


class UserCommonInformationSaveView(APIView):
    authentication_classes = (JSONWebTokenAuthentication,)

    def post(self, request, *args, **kwargs):
        data = request.data
        user_id = data['id']
        user = User.objects.get(id=user_id)
        serializer = UserMeSerializer(user, data=data, partial=True)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
        passport_id = data['documents']['passport']['id']
        passport_instance = DocumentToUser.objects.get(id=passport_id)

        serializer = DocumentSerializer(passport_instance, data=data['documents']['passport'], partial=True)
        if serializer.is_valid(raise_exception=True):
            serializer.save()

        return Response({})
