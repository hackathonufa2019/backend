from django.contrib import admin

from .models import DocumentType, DocumentToUser


@admin.register(DocumentType)
class DocumentTypeAdmin(admin.ModelAdmin):
    pass


@admin.register(DocumentToUser)
class DocumentToUserAdmin(admin.ModelAdmin):
    pass
