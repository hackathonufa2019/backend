from django.contrib.postgres.fields import JSONField
from django.db import models


class DocumentType(models.Model):
    # Можно через
    name = models.CharField(max_length=128, unique=True)

    def __str__(self):
        return str(self.name)


class DocumentToUser(models.Model):
    UNKNOWN = -1
    INVALID = 0
    VALID = 1
    DOC_VALID_CHOICES = (
        (UNKNOWN, 'Не проверено'),
        (INVALID, 'Невалидно'),
        (VALID, 'Валидно'),

    )

    user = models.ForeignKey('user.User', on_delete=models.CASCADE,
                             related_name='documents')
    doc_type = models.ForeignKey(DocumentType, on_delete=models.PROTECT,
                                 related_name='type_documents')
    # JSON Не работает в sqlite
    document_data = JSONField(blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    valid = models.IntegerField(choices=DOC_VALID_CHOICES, default=UNKNOWN)

    def __str__(self):
        return f'{str(self.user)} - {str(self.doc_type)}'
